//
//  SharedApiManager.swift
//  TMobile
//
//  Created by dilshad Mohammad on 05/03/20.
//


import Foundation

struct SharedApiManager {
    
    typealias Completion = (_ response: Any?, _ error: Error?) ->()
    
     static let shared =  SharedApiManager()
    
     private init() {
        
    }
    
    
    func getUsers(urlString: String,completionHandler: @escaping Completion) {
        self.getDataForUrl(urlString: urlString, parameters: nil) { (response, error) in
            if error != nil {
                print("Error")
                return
            }
            if let data = response as? Data {
                do {
                    let users = try JSONDecoder().decode([User].self, from: data)
                    completionHandler(users, nil)
                } catch {
                    print("Failed to parse the data")
                    completionHandler(nil, error)
                }
            }
        }
        
    }
    
    func getUserDetails(urlString: String, completionHandler: @escaping Completion) {
        self.getDataForUrl(urlString: urlString, parameters: nil) { (response, error) in
            if error != nil {
                print("Error")
                return
            }
            if let data = response as? Data {
                do {
                    let userDetails = try JSONDecoder().decode(UserDetail.self, from: data)
                    completionHandler(userDetails, nil)
                } catch {
                    print("Failed to parse the data")
                    completionHandler(nil, error)
                }
            }
        }
        
    }
    
    func getUserRepos(urlString: String, completionHandler: @escaping Completion) {
        self.getDataForUrl(urlString: urlString, parameters: nil) { (response, error) in
            if error != nil {
                print("Error")
                return
            }
            if let data = response as? Data {
                do {
                    let userDetails = try JSONDecoder().decode([Repo].self, from: data)
                    completionHandler(userDetails, nil)
                } catch {
                    print("Failed to parse the data")
                    completionHandler(nil, error)
                }
            }
        }
        
    }
    
    func getDataForUrl(urlString: String, parameters: [String : Any]?, completionHandler: @escaping Completion) {
        guard let url = URL(string: urlString) else {
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error != nil {
                print(error!)
              completionHandler(nil, error)
            } else {
                if let httpResp = response as? HTTPURLResponse {
                    switch httpResp.statusCode {
                    case 200..<300:
                        completionHandler(data, nil)
                        break
                    default:
                        completionHandler(nil, error)
                        break
                    }
                }
            }
        }.resume()
    }
    
    
}
