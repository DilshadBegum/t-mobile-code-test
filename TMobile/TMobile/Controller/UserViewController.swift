//
//  UserViewController.swift
//  TMobile
//
//  Created by dilshad Mohammad on 05/03/20.
//


import UIKit
import SDWebImage

class UserViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var usersTableView: UITableView!
    var usersArray: [User] = []
    var filteredArray: [User] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        usersTableView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        usersTableView.rowHeight = UITableView.automaticDimension
        usersTableView.estimatedRowHeight = 50
        usersTableView.tableFooterView = UIView(frame: .zero)
        usersTableView.isHidden = true
        searchBar.isHidden = true
        getUsersData()
        searchBar.delegate = self
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell =  tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell") as? UserTableViewCell {
            let user = filteredArray[indexPath.row]
            cell.userName.text = user.login
            cell.usersNoOfRepos.text = ""
            if let url = URL(string: user.avatarUrl ?? "") {
                cell.userImage.sd_setImage(with: url) { (image, error, SDImageCacheType, url) in
                    cell.userImage.image = image
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let user = filteredArray[indexPath.row]
    self.performSegue(withIdentifier: "userDetailView", sender: user)
    tableView.deselectRow(at: indexPath, animated: true)
    addBackButtonTitle()
  }
  
  func addBackButtonTitle() {
    let backItem = UIBarButtonItem()
    backItem.title = "Back"
    navigationItem.backBarButtonItem = backItem
  }
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailView = segue.destination as? UserDetailViewController {
            detailView.user = (sender as? User)
        }
    }
    
  func getUsersData() {
    ProgressView.show()
    SharedApiManager.shared.getUsers(urlString: "https://api.github.com/users") {[weak self] (response, error) in
      if let users =  response as? [User] {
        self?.usersArray = users
        self?.filteredArray = users
        DispatchQueue.main.async {
          ProgressView.dismiss()
          self?.usersTableView.reloadData()
          self?.usersTableView.isHidden = false
          self?.searchBar.isHidden = false
        }
      } else {
        DispatchQueue.main.async {
          ProgressView.dismiss()
          let alertController = UIAlertController(title: "", message: "Something went wrong!", preferredStyle: .alert)
          alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
          }))
          self?.present(alertController, animated: true, completion: nil)
        }
      }
    }
  }

}

extension UserViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filteredArray = usersArray.filter({ $0.login?.range(of: searchText, options: .caseInsensitive) != nil })
        if searchText.count == 0 {
            self.filteredArray = usersArray
        }
        self.usersTableView.reloadData()
    }
}
