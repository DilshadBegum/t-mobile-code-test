//
//  UserDetailViewController.swift
//  TMobile
//
//  Created by dilshad Mohammad on 05/03/20.
//

import UIKit

class UserDetailViewController: UIViewController {
  
  @IBOutlet weak var userImageView: UIImageView!
  @IBOutlet weak var userDetailLabel: UILabel!
  @IBOutlet weak var detailTableView: UITableView!
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var biographyLabel: UILabel!
  var user: User?
  var userDetails: UserDetail?
  var repos: [Repo] = []
  var filteredRepos: [Repo] = []
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.title = "GitHub Searcher"
    detailTableView.rowHeight = UITableView.automaticDimension
    detailTableView.estimatedRowHeight = 50
    detailTableView.isHidden = true
    userImageView.isHidden = true
    searchBar.isHidden = true
    detailTableView.tableFooterView = UIView(frame: .zero)
    self.getUserDetails()
    searchBar.delegate = self
  }
  
  func updateUI() {
    if let url = URL(string: self.userDetails?.avatarUrl ?? "") {
      self.userImageView.sd_setImage(with: url) { (image, error, SDImageCacheType, url) in
        self.userImageView.image = image
      }
    }
    var detailStr = ""
    if let login = self.userDetails?.login, !login.isEmpty {
      detailStr += "\(login)\n"
    }
    if let email = self.userDetails?.email, !email.isEmpty {
      detailStr += "\(email)\n"
    }
    if let location = self.userDetails?.location, !location.isEmpty {
      detailStr += "\(location)\n"
    }
    if let createdAt = self.userDetails?.createdAt, !createdAt.isEmpty {
      detailStr += "\(createdAt)\n"
    }
    if let followers = self.userDetails?.followers, followers > 0 {
      detailStr += "\(followers) Followers\n"
    }
    if let following = self.userDetails?.following, following > 0 {
      detailStr += "Following \(following)\n"
    }
    self.userDetailLabel.text = detailStr
    self.biographyLabel.text = self.userDetails?.bio
  }
  
  func getUserDetails() {
    let dispatchGroup = DispatchGroup()
    ProgressView.show()
    dispatchGroup.enter()
    SharedApiManager.shared.getUserDetails(urlString: "https://api.github.com/users/\(self.user?.login ?? "")") {[weak self] (response, error) in
      if let user =  response as? UserDetail {
        self?.userDetails = user
      }
      dispatchGroup.leave()
    }
    
    dispatchGroup.enter()
    SharedApiManager.shared.getUserRepos(urlString: self.user?.reposUrl ?? "") {[weak self] (response, error) in
      if let repo =  response as? [Repo] {
        self?.repos = repo
        self?.filteredRepos = repo
      }
      dispatchGroup.leave()
    }
    
    dispatchGroup.notify(queue: .main) {
      ProgressView.dismiss()
      self.updateUI()
      self.detailTableView.reloadData()
      self.detailTableView.isHidden = false
      self.userImageView.isHidden = false
      if self.repos.count > 0 {
        self.searchBar.isHidden = false
      }
      if self.userDetails == nil {
        let alertController = UIAlertController(title: "", message: "Something went wrong!", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
          self.navigationController?.popViewController(animated: true)
        }))
        self.present(alertController, animated: true, completion: nil)
      }
    }
  }
  
}


extension UserDetailViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return filteredRepos.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if let cell =  tableView.dequeueReusableCell(withIdentifier: "RepoTableViewCell") as? RepoTableViewCell {
      let repo = filteredRepos[indexPath.row]
      cell.repoNameLabel.text = repo.name
      cell.forksLabel.text = "\(repo.forks ?? 0) Forks"
      cell.starsLabel.text = "\(repo.stargazersCount ?? 0) Stars"
      return cell
    }
    return UITableViewCell()
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let repo = filteredRepos[indexPath.row]
    if let urlStr = repo.htmlUrl, let url = URL(string: urlStr) {
      UIApplication.shared.open(url)
    }
    tableView.deselectRow(at: indexPath, animated: true)
  }
}

extension UserDetailViewController: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    self.filteredRepos = repos.filter({ $0.name?.range(of: searchText, options: .caseInsensitive) != nil })
    if searchText.count == 0 {
      self.filteredRepos = repos
    }
    self.detailTableView.reloadData()
  }
}
