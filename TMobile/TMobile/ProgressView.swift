//
//  ProgressView.swift
//  TMobile
//
//  Created by dilshad Mohammad on 05/03/20.
//

import UIKit
import MBProgressHUD
class ProgressView {
    let shared = ProgressView()
    private init() {}
    
    static func show() {
        if let window =  UIApplication.shared.windows.first {
            MBProgressHUD.showAdded(to: window, animated: true)
        }
    }
 
    static func dismiss() {
         if let window =  UIApplication.shared.windows.first {
            MBProgressHUD.hide(for: window, animated: true)
        }
    }
}
