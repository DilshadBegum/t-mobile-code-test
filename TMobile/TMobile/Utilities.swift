//
//  Utilities.swift
//  TMobile
//
//  Created by dilshad Mohammad on 05/03/20.
//

import UIKit

class Utilities: NSObject {
    static func getDateFromString(_ dateStr: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ssZ"
        let date = dateFormatter.date(from: dateStr) ?? Date()
        dateFormatter.dateFormat = "MMM dd yyyy"
        return dateFormatter.string(from: date)
    }
}
